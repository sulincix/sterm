public class NotifcationWidget : Gtk.Label{
    public NotifcationWidget(){
        this.get_style_context().add_class("notification");
        blocked = false;
    }
    private bool status;
    public bool blocked;
    public void show_text(string txt){
        show_text_time(txt,txt.length*100);
    }
    public void show_text_time(string txt, int time){
        if(blocked){
            hide_text();
        }
        this.set_label(txt);
        if(status){
            return;
        }
        status=true;
        this.show();
        GLib.Timeout.add(time,hide_text);
    }
    
    public bool hide_text(){
        status=false;
        this.hide();
        return false;
    }
}
