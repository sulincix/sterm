#if no_locale
public string _(string msg){
    return msg;
}
#else
public const string GETTEXT_PACKAGE="sterm";
#endif

int main (string[] args) {
    Intl.setlocale (LocaleCategory.ALL, "");
    Intl.bindtextdomain (GETTEXT_PACKAGE, "/usr/share/locale");
    Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (GETTEXT_PACKAGE);
    GLib.Settings settings = new GLib.Settings("org.sulincix.sterm");
    if(settings.get_boolean("xwayland")){
        Gdk.set_allowed_backends("x11");
    }
    GLib.Environment.set_prgname("org.sulincix.sterm");
    Gtk.init(ref args);
    var window = new MainWindow(args);
    window.set_icon_name("sterm");
    window.reload_theme();
    window.create_tab();
    Gtk.main();
    return 0;
}
