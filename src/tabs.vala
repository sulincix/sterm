public class Scrolled : Gtk.ScrolledWindow {
    public Gtk.Widget widget;
    
    public void set_widget(Gtk.Widget w){
        widget = w;
        this.get_style_context().add_class("scrolled");
        this.add(widget);
    }
}

public class TabButton : Gtk.Box {

    public signal void click_event();
    private TabView tabs;
    public Gtk.Button tablabel;
    private int max_title_len=31;
    public TabButton(TabView t){
        //widgets
        tabs = t;
        tablabel = new Gtk.Button.with_label(name);
        tablabel.set_relief(Gtk.ReliefStyle.NONE);
        tablabel.get_style_context().add_class("transparent");
        //main box
        this.pack_start(tablabel,false,true,0);
        tablabel.clicked.connect((but)=>{
            click_event();
        });
        this.set_can_focus(false);
        this.show_all();
        this.get_style_context().add_class("tab_button");
    }
    private string title;
    private string ftitle;
    private bool string_handler_activated = false;
    public void set_title(string new_title){
        title = new_title;
        ftitle = new_title;
        if(tabs.get_visible_child_name() != this.name){
            return;
        }
        if(title.length >= max_title_len){
            title += "  -  ";
            if(!string_handler_activated){
                string_handler_activated = true;
                GLib.Timeout.add(0,string_handler);
            }
        }else{
            string_handler_activated = false;
        }
        tabs.window.set_title(title);
        tabs.current_tab_label.set_label(ftitle);
    }
    int cur = 0;
    public bool string_handler(){
        if(title == null){
            return true;
        }
        if(tabs.get_visible_child_name() != this.name){
            return true;
        }
        if(title.length >= max_title_len){
            if(cur >= title.length ){
                cur =0;
            }
            string new_title = title.substring(cur,title.length - cur) + title.substring(0,cur);
            new_title = new_title.substring(0, max_title_len -1);
            cur += 1;
            tabs.current_tab_label.set_label(new_title);
            GLib.Timeout.add(200,string_handler);
        }
        return false;
    }
    public string get_title(){
        return ftitle;
    }
    public void set_active(bool status){
        if(status){
            this.get_style_context().add_class("active_tab");
            this.get_style_context().remove_class("tab_button");
        }else{
            this.get_style_context().remove_class("active_tab");
            this.get_style_context().add_class("tab_button");
        }
    }
}

public class TabView : Gtk.Stack {

    public MainWindow window;
    public Gtk.Label current_tab_label;
    public TabView(MainWindow w){
        window = w;
        // tab box define
        tabbox = new Gtk.Box(Gtk.Orientation.HORIZONTAL,13);
        tabbox.set_can_focus(false);
        tabbox.get_style_context().add_class("tabbox");
        // new tab button
        Gtk.Button new_tab = new Gtk.Button();
        new_tab.set_label("+");
        new_tab.clicked.connect((but)=>{
            window.create_tab();
        });
        new_tab.get_style_context().add_class("tab_button");
        new_tab.get_style_context().add_class("active_tab");
        // current tab label
        current_tab_label = new Gtk.Label("sterm");

        // tab box features
        tabbox.pack_end(current_tab_label,false,false,0);
        tabbox.pack_start(new_tab,false,false,0);
        tabbox.get_style_context().add_class("window_box");
        this.show_all();
        ((Gtk.Widget)this).child_notify.connect((pri)=>{
            string name = this.get_visible_child_name();
            if (name != null){
                set_active_tab(name);
            }
        });
        this.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT);
     }
    private TabButton[] bs;
    private string[] removed;
    public Gtk.Box tabbox;
    public void create_tab(Gtk.Widget widget, string name){
        Scrolled scrolled = new Scrolled();
        scrolled.set_widget(widget);
        this.add_titled(scrolled, name, name);
        TabButton b = new TabButton(this);
        b.set_active(false);
        if(bs == null){
            bs = {};
        }
        if(removed == null){
            removed = {};
        }
        bs += b;
        b.set_title(name);
        b.name = name;
        b.click_event.connect(()=>{
            set_active_tab(name);
        });
        scrolled.show_all();
        this.set_visible_child_name(name);
        if(tabbox != null){
            tabbox.pack_start(b,false,false,0);
        }
        tabbox.show_all();
        update_tabbutton_label();
    }
    public void set_tab_title(string name, string new_title){
        foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            if(b.name == name){
                b.set_title(new_title);
            }
        }
    }
    public Gtk.Widget[] get_all_widgets(){
        Gtk.Widget[] ret = {};
        foreach(TabButton b in bs){
            var name = b.name;
            if(!is_valid_tab(name)){
                continue;
            }
            ret += ((Scrolled)get_child_by_name(name));
        }
        return ret;
    }
    public bool is_valid_tab(string name){
        foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }else if(b.name == name){
                return true;
            }
        }
        return false;
    }
    public void set_active_tab(string name){
        if(name == null){
            return;
        }else if(name in removed){
            return;
        }else if(!is_valid_tab(name)){
            return;
        }
        var scr = ((Scrolled)get_child_by_name(name));
        if(scr == null){
            return;
        }
        this.set_visible_child_name(name);
        foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            b.set_active(false);
        }
        scr.widget.grab_focus();
        TabButton bx = get_tabbutton_by_name(name);
        bx.set_active(true);
        bx.set_title(bx.get_title());
        update_tabbutton_label();
        window.notification.show_text(_("Current Tab: %d").printf(get_tab_index(name)));
        
    }
    public TabButton get_tabbutton_by_name(string name){
        foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            if(b.name == name){
                return b;
            }
        }
        return new TabButton(this);
    }
    
    public int get_tab_index(string name){
        int i=0;
        foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            if(b.name == name){
                return i;
            }
            i+=1;
        }
        return i;
    }
    public string get_tab_by_index(int index){
         int i=0;
         if(index < 0){
             index = 0;
         }
         foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            if(index == i){
                return b.name;
            }
            i+=1;
        }
        return "";
    }
    public int get_num_of_pages(){
         int i=0;
         foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            i+=1;
        }
        return i;
    }
    public void remove_tab(string name){

            // get widgets
            Gtk.Widget widget = get_child_by_name(name);
            TabButton but = get_tabbutton_by_name(name);
            removed += name;

            // go previous tab
            int index = get_tab_index(name) -1;
            if(index < 0){
                index = 0;
            }

            // remove widgets
            if(widget != null){
                tabbox.remove(but);
                this.remove(widget);
            }

            set_active_tab(get_tab_by_index(index));
            update_tabbutton_label();

            // set active page previous
            if(get_num_of_pages() <= 0){
                Gtk.main_quit();
            }
            window.reload_theme();
    }
    public void update_tabbutton_label(){
        int i=0;
         foreach(TabButton b in bs){
            if(b == null || b.name in removed){
                continue;
            }
            b.tablabel.set_label(i.to_string());
            i+=1;
        }
    }
}
