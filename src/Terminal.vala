public class Terminal : Vte.Terminal{

    private TabView tabs;
    private string[] args;
    public Terminal(string[] a) {
        args = {};
        #if FLATPAK
        // uses host-spawn
        args += "/app/bin/host-spawn";
        #endif
        foreach (string aa in a){
            args += aa;
        }
        this.set_scrollback_lines(-1);
        this.show_all();
        this.init();
        define_shortcuts();
    }
    
    public void init(){
        #if DEPRECATED
        try {
            this.spawn_sync (Vte.PtyFlags.DEFAULT,null, args,null,0,null,null);
        } catch(GLib.Error e){
            stderr.printf(e.message);
        }
        #else
        this.spawn_async(Vte.PtyFlags.DEFAULT, null, args, null, 0, null, -1, null, null);
        #endif
    }
    public void connect_tabview(TabView t){
        tabs = t;
        tabs.set_tab_title(name,this.window_title);
        this.child_exited.connect(()=>{
            tabs.remove_tab(name);
        });
        this.window_title_changed.connect(()=>{
            tabs.set_tab_title(name,this.window_title);
        });
    }
    
    public void set_color(int r, int g, int b, double a){
        var color = Gdk.RGBA();
        color.red = (double)r/255;
        color.blue = (double)b/255;
        color.green = (double)g/255;
        color.alpha = (double)a/255;
        this.set_color_background(color);
    }
    public void set_foreground_color(int r, int g, int b, double a){
        var color = Gdk.RGBA();
        color.red = (double)r/255;
        color.blue = (double)b/255;
        color.green = (double)g/255;
        color.alpha = (double)a/255;
        this.set_color_foreground(color);
    }
    
    private void define_shortcuts(){
        // copy event
        var copy_event = new KeyEvent();
        copy_event.keycode = KEY.C;
        copy_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        copy_event.event.connect(()=>{
            tabs.window.menu.copy_clipboard_event(this);
        });
        add_hotkey_event(copy_event);
        // copy event
        var guard_event = new KeyEvent();
        guard_event.keycode = KEY.c;
        guard_event.state = Gdk.ModifierType.CONTROL_MASK;
        int64 last = 0;
        guard_event.event.connect(()=>{
            int64 cur = get_epoch();
            if (300 >= cur - last){
                guard_event.blocked = false;
                tabs.window.notification.hide_text();
            }else{
                tabs.window.notification.show_text_time(_("Ctrl-C blocked.\nPress it twice within 300ms if you need it."),5000);
                guard_event.blocked = true;
            }
            last = cur;
        });
        add_hotkey_event(guard_event);
        // paste event
        var paste_event = new KeyEvent();
        paste_event.keycode = KEY.V;
        paste_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        paste_event.event.connect(()=>{
             tabs.window.menu.paste_clipboard_event(this);

        });
        add_hotkey_event(paste_event);
    }

}
