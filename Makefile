build: clean
	meson setup build -Ddebug=false -Ddeprecated=false --prefix=/usr
	ninja -C build

run: debug
	install data/gsettings.xml build/org.sulincix.sterm.gschema.xml
	glib-compile-schemas build
	GSETTINGS_SCHEMA_DIR=build ./build/sterm

debug: clean
	meson setup build -Ddebug=true --prefix=/usr
	ninja -C build

install:
	ninja -C build install

clean:
	rm -rf build ctx.vala po/*.mo

pot:
	xgettext -o po/sterm.pot --from-code="utf-8" `find theme src -type f -iname "*.vala"` `find src -type f -iname "*.c"`
	for file in `ls po/*.po`; do \
	        msgmerge $$file po/sterm.pot -o $$file.new ; \
	    echo POT: $$file; \
	    rm -f $$file ; \
	    mv $$file.new $$file ; \
	done
