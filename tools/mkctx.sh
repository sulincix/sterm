#!/bin/bash
set -e
> $1
ls theme/*.css src/*.css | while read line; do
    echo -e "public string get_$(basename $line | sed "s/\./_/g") (){\n    return \"" >> $1
    cat $line | sed 's|\\|\\\\|g' | sed "s|\"|\\\\\"|g" >> $1
    echo -e "\";\n}" >> $1
done
