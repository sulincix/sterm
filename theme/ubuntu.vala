public class theme_ubuntu : color_theme {
    public theme_ubuntu(){
        name = "Ubuntu";
        description = _("Ubuntu");
        bg_red = 48;
        bg_green = 9;
        bg_blue =  36;
        fg_red = 255;
        fg_green = 255;
        fg_blue = 255;
        palette = {
            color("#2f3237"), // black
            color("#cc0001"), // red
            color("#4c9a05"), // green
            color("#c79e04"), // yellow
            color("#3166a9"), // blue
            color("#75507c"), // magenta
            color("#05999b"), // cyan
            color("#d1d7d2"), // white
            // bright
            color("#525852"), // black
            color("#e92c27"), // red
            color("#86e23d"), // green
            color("#fce84f"), // yellow
            color("#789ad7"), // blue
            color("#ac7faa"), // magenta
            color("#30e3e5"), // cyan
            color("#eeeeec"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid #273b4e;
          }
        ";

    }
}
