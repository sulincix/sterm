public class theme_nord : color_theme {
    public theme_nord(){
        name = "Nord";
        description = _("Nord");
        bg_red = 41;
        bg_green = 49;
        bg_blue =  62;
        fg_red = 229;
        fg_green = 233;
        fg_blue = 240;
        palette = {
            color("#273b4e"), // black
            color("#be6069"), // red
            color("#a6be8b"), // green
            color("#ecca88"), // yellow
            color("#82a4c4"), // blue
            color("#b490af"), // magenta
            color("#8bc2d2"), // cyan
            color("#e6eaf1"), // white
            // bright
            color("#384c5f"), // black
            color("#cf717a"), // red
            color("#b7ce9c"), // green
            color("#fddb99"), // yellow
            color("#93b5d5"), // blue
            color("#c5a1bf"), // magenta
            color("#9cd3e3"), // cyan
            color("#f7fbf2"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid #273b4e;
          }
        ";

    }
}
