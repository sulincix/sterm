public string theme;

public Gdk.RGBA color(string c){
    var rgba = Gdk.RGBA();
    rgba.parse(c);
    return rgba;
}

public int alpha;

public class color_theme {
    public string name = "default";
    public string description = _("Dark");
    public int bg_red = 0;
    public int bg_green = 0;
    public int bg_blue = 0;
    public int fg_red = 255;
    public int fg_green = 255;
    public int fg_blue = 255;
    public string css = "";
    public Gdk.RGBA[] palette;

    public color_theme(){
        palette = {
            color("#313131"),
            color("#cc0000"),
            color("#00cc00"),
            color("#cccc00"),
            color("#0000cc"),
            color("#cc00cc"),
            color("#00cccc"),
            color("#cccccc"),
            // bright
            color("#424242"),
            color("#ff0000"),
            color("#00ff00"),
            color("#ffff00"),
            color("#0000ff"),
            color("#ff00ff"),
            color("#00ffff"),
            color("#ffffff"),
        };
    }
    
    public string build() {
      string ret = "
      * {
        color: rgba(@fr, @fg, @fb, 1);
      }
      .notification {
        background: rgba(@br, @bg, @bb, @a@);
        border: 1px solid rgba(@fr, @fg, @fb, @a@);
      }
      .window_box {
        background: rgba(@br, @bg, @bb, @a@);
      }
      .main_box {
        background: rgba(@br, @bg, @bb, @a@);
      }
      headerbar {
        background: rgba(@br, @bg, @bb, @a@);
        color: rgba(@fr, @fg, @fb, 1);
      }
      .blocker {
          background: rgba(@br, @bg, @bb, 0.40);
      }" + css;
      ret = ret.replace("@br",bg_red.to_string());
      ret = ret.replace("@bg",bg_green.to_string());
      ret = ret.replace("@bb",bg_blue.to_string());
      ret = ret.replace("@fr",fg_red.to_string());
      ret = ret.replace("@fg",fg_green.to_string());
      ret = ret.replace("@fb",fg_blue.to_string());
      ret = ret.replace("@a@",((double)alpha/ 255).to_string());
      #if DEBUG
      stdout.printf(ret);
      #endif
      return ret;
    }
    
}
public color_theme current_theme ;
public void set_theme(string name){
    theme = name;
    if(name == "random"){
        current_theme = new theme_random();
    }
    get_theme_css();
}
private color_theme[] color_themes;

public void theme_init(){
    if(color_themes == null){
        color_themes = {};
    }
    color_themes += new theme_native();
    color_themes += new color_theme();
    color_themes += new theme_light();
    color_themes += new theme_lighter();
    color_themes += new theme_darker();
    color_themes += new theme_shit();
    color_themes += new theme_ubuntu();
    color_themes += new theme_solarized();
    color_themes += new theme_solarized_light();
    color_themes += new theme_adwaita_light();
    color_themes += new theme_adwaita();
    color_themes += new theme_dracula();
    color_themes += new theme_amogus();
    color_themes += new theme_nord();
    color_themes += new theme_midnight();
    color_themes += new theme_sweetlove();
    color_themes += new theme_hacker();
    color_themes += new theme_random();
}

public string get_theme_css(){
    if(theme == null){
        theme = "default";
    }
    foreach(color_theme t in color_themes){
        if (t.name == theme && theme != "random"){
            current_theme = t;
            break;
        }
    }
    if(current_theme == null){
        current_theme = new color_theme();
    }
    return current_theme.build();
}

