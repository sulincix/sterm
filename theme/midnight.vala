public class theme_midnight : color_theme {
    public theme_midnight(){
        name = "midnight";
        description = _("Midnight");
        bg_red = 26;
        bg_green = 37;
        bg_blue =  47;
        fg_red = 236;
        fg_green = 240;
        fg_blue = 241;
        palette = {
            color("#2c3e50"), // black
            color("#e74c3c"), // red
            color("#2ecc71"), // green
            color("#f1c40f"), // yellow
            color("#3498db"), // blue
            color("#9b59b6"), // magenta
            color("#1abc9c"), // cyan
            color("#ecf0f1"), // white
            // bright
            color("#34495e"), // black
            color("#ff6b6b"), // red
            color("#2ed573"), // green
            color("#ffeaa7"), // yellow
            color("#74b9ff"), // blue
            color("#a29bfe"), // magenta
            color("#00b894"), // cyan
            color("#ffffff"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid black;
          }
        ";

    }
}
