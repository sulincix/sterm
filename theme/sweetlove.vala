public class theme_sweetlove : color_theme {
    public theme_sweetlove(){
        name = "sweetlove";
        description = _("Sweetlove");
        bg_red = 31;
        bg_green = 31;
        bg_blue =  31;
        fg_red = 192;
        fg_green = 177;
        fg_blue = 139;
        palette = {
            color("#4a3637"), // black
            color("#d17b49"), // red
            color("#7b8748"), // green
            color("#af865a"), // yellow
            color("#535c5c"), // blue
            color("#775759"), // magenta
            color("#6d715e"), // cyan
            color("#c0b18b"), // white
            // bright
            color("#402e2e"), // black
            color("#ac5d2f"), // red
            color("#647035"), // green
            color("#8f6840"), // yellow
            color("#444b4b"), // blue
            color("#614445"), // magenta
            color("#585c49"), // cyan
            color("#978965"), // white
        };

	css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid #4a3637;
          }
	";

    }
}
